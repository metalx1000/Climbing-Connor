extends Control

var time = 0

func _ready():
	yield(get_tree().create_timer(.5),"timeout")
	time = 1
	Global.number_of_players = 1
	pass # Replace with function body.

func _process(delta):
	if Input.is_action_just_pressed("ui_accept") && time > 0:
		$continue.play("continue")
		
func next():
	get_tree().change_scene("res://scenes/cut_scenes/Cut_Scene_1.tscn")
		
