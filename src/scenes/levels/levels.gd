extends Node2D

export(String,FILE,"*.tscn") var next_level = "res://scenes/main_title.tscn"

var players = preload("res://objects/player/player.tscn")
var unlocked = false
export (int) var gravity = 3000

func _ready():
	for player in get_tree().get_nodes_in_group("players"):
		player.gravity = gravity
	#print("Number of Players " + str(Global.number_of_players))
	check_2players()
	pass # Replace with function body.


func _input(event):
	if event.is_action_pressed("new_player"):
		Global.number_of_players += 1
		check_2players()

func check_2players():
	if get_tree().get_nodes_in_group("players").size() < 2:
		var player_2 = players.instance()
		player_2.gravity = gravity + 20
		player_2.player_id = "2"
		player_2.position = get_tree().get_nodes_in_group("players")[0].start_pos
		player_2.position.x += 32
		add_child(player_2)
