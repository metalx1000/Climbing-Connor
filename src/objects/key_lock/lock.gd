extends Area2D


func _ready():
	visible = false
	
func _process(delta):
	if owner.unlocked:
		visible = true
	else:
		visible = false

func _on_lock_body_entered(body):
	if body.is_in_group("players") && visible:
		$AnimationPlayer.play("complete")
		body.queue_free()
		
func next_level():
	get_tree().change_scene(owner.next_level)
