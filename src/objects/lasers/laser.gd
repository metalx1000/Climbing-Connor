extends Area2D

var dir = 1
var speed = 300

func _ready():
	pass # Replace with function body.

func _process(delta):
	
	position.x += speed * delta * dir
#	pass


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_laser_body_entered(body):
	if body.is_in_group("players") && visible:
		body.death()
