extends Node2D

var lasers = preload("res://objects/lasers/laser.tscn")
export var rate = 5
export var speed = 300



func _ready():
	$Timer.wait_time = rate

func shoot():
	var laser = lasers.instance()
	laser.speed = speed
	#laser.position = position
	add_child(laser)

