extends Area2D


export (int) var id = 0
var active = 0

func _ready():
	pass # Replace with function body.

func _process(delta):
	if active > 0:
		active -= delta
		
func _on_teleporter_body_entered(body):
	if active > 0:
		return
		
	var teleporters = get_tree().get_nodes_in_group("teleporters")
	for t in teleporters:
		if t != self && t.id == id:
			t.active = .2
			body.position = t.position
			break
