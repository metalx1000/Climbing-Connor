extends CanvasLayer

const MSG_RATE = 0.5

onready var textbox_container = $Dialog/TextboxContainer
onready var start_symbol = $Dialog/TextboxContainer/MarginContainer/HBoxContainer/Start
onready var end_symbol = $Dialog/TextboxContainer/MarginContainer/HBoxContainer/End
onready var label = $Dialog/TextboxContainer/MarginContainer/HBoxContainer/Label

enum State {
	READY,
	READING,
	FINISHED
}

var current_state = State.READY
var text_queue = []

var image_num = 0

func _ready():
	$connor.visible = false
	$ember.visible = false
	hide_textbox()
	queue_text("HAHAHAHA!!!",Color(1,0,0))
	queue_text("That was a Crazy Slide!")
	queue_text("If you liked that, You'll LOVE this.",Color(1,0,0))
	queue_text("Oh Brother")
	
func _process(delta):
	check_state()
	if image_num % 2:
		$connor.visible = false
		$ember.visible = true
	else:
		$ember.visible = false
		$connor.visible = true
		
func check_state():
	match current_state:
		State.READY:
			if !text_queue.empty():
				display_text()
			else:
				get_tree().change_scene("res://scenes/levels/Level_06.tscn")
		State.READING:
			if Input.is_action_just_pressed("continue"):
				label.percent_visible = 1.0
				$Tween.stop_all()
				end_symbol.text = "v"
				change_state(State.FINISHED)
		State.FINISHED:
			if Input.is_action_just_pressed("continue"):
				change_state(State.READY)
				hide_textbox()
				

func queue_text(next_text,color=Color(1,1,1)):
	text_queue.push_back([next_text,color])

func hide_textbox():
	image_num += 1
	start_symbol.text = ""
	end_symbol.text = ""
	label.text = ""
	textbox_container.hide()

func show_textbox():
	start_symbol.text = "*"
	textbox_container.show()

func display_text():
	var next_text = text_queue.pop_front()
	label.text = next_text[0]
	label.set("custom_colors/font_color",next_text[1])
	label.percent_visible = 0.0
	change_state(State.READING)
	show_textbox()
	$Tween.interpolate_property(label, "percent_visible", 0.0, 1.0, len(next_text) * MSG_RATE, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()

func change_state(next_state):
	current_state = next_state

func _on_Tween_tween_completed(object, key):
	end_symbol.text = "v"
	change_state(State.FINISHED)
